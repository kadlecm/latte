<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '48d916b0b5f7b703be735a029aa17b61dbe72b23',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '48d916b0b5f7b703be735a029aa17b61dbe72b23',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'latte/latte' => array(
            'pretty_version' => 'v3.0.13',
            'version' => '3.0.13.0',
            'reference' => '462444d669809528b6f6ce191b616d747c9b4bfc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../latte/latte',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
